DATE := $(shell date +%y%m%d)
.PHONY: clean cancionero.pdf impresion.pdf

cancionero.pdf :
	pdflatex cancionero.tex < /dev/null
	luatex songidx.lua idxfile.sxd idxfile.sbx
	luatex songidx.lua idxfile-authors.sxd idxfile-authors.sbx
	pdflatex cancionero.tex < /dev/null

generate-acordes : set-acordes cancionero.pdf
	mkdir -p public/$(DATE)
	mv cancionero.pdf public/$(DATE)/cancionero_acordes_$(DATE).pdf

generate-letra :  set-letra cancionero.pdf
	mkdir -p public/$(DATE)
	mv cancionero.pdf public/$(DATE)/cancionero_letra_$(DATE).pdf

impresion.pdf : set-letra cancionero.pdf
	mkdir -p public/$(DATE)
	pdflatex impresion.tex < /dev/null
	cp impresion.pdf public/$(DATE)/cancionero_impresion_$(DATE).pdf

set-acordes :
	sed -i 's@{estilo/[a-zA-Z]*}@{estilo/acordes}@' cancionero.tex

set-letra :
	sed -i 's@{estilo/[a-zA-Z]*}@{estilo/letra}@' cancionero.tex

clean :
	rm -f *.gz *.aux *.log *.sxd *.sbx *.toc *.log *.sxc *.out
