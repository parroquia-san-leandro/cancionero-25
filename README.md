# Cancionero 25 aniversario

## Sobre este cancionero

Este cancionero está escrito en un sistema que se llama [LaTeX][6]. LaTeX es una forma de escribir documentos un poco distinta a la habitual. Estamos acostumbrados a escribir el texto y mediante botones darle estilo (negrita, subrayado, ajuste de márgenes).
TODO completar

## Cómo editar el cancionero

**Nota**: Para las instrucciones, las numeradas deben seguirse de forma secuencial, y las no numeradas son opciones, de las que solo es necesario escoger una.

1. [Descarga][4] un archivo comprimido del cancionero de la [página principal][5] del proyecto.
1. Encuentra un entorno para editar LaTeX
    * Online
        1. Abre [Overleaf][3] en cualquier navegador.
        2. Crea una cuenta o inicia sesión.
        3. Crea un nuevo proyecto y selecciona subir existente.
        4. Sube el archivo comprimido (zip) que has descargado.
    * En tu ordenador
        1. [Instala][1] una distribución de LaTeX. En Windows el más sencillo de instalar es MiKTeX.
        2. Instala un editor, como por ejemplo [TexStudio][2]. Se puede editar sin editor, pero resulta más complicado para principiantes.
2. Abre el archivo principal: cancionero.tex
3. Realiza un cambio, como corregir la letra de una canción, cambiar el tamaño del papel o el orden y numeración de las canciones.
4. Compila el archivo (dale al botón de "Play") y verás el PDF generado. El documento se encuentra en la carpeta, de donde se puede copiar a cualquier sitio.
    1. Si quieres que los índices alfabéticos y de autor salgan correctamente, hay que realizar ciertos pasos extra. Básicamente, hay que ejecutar los comandos del archivo `Makefile`. TODO completar

[1]: https://www.latex-project.org/get/
[2]: https://www.texstudio.org/
[3]: https://overleaf.com/
[4]: https://gitlab.com/parroquia-san-leandro/cancionero-25/-/archive/master/cancionero-25-master.zip
[5]: https://gitlab.com/parroquia-san-leandro/cancionero-25
[6]: https://es.wikipedia.org/wiki/LaTeX
